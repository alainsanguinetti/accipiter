��          <      \       p   )   q   q   �   4       B  %   \  }   �  3                       Accipiter - the simplest webshop on Earth This webshop let you generate an email that you can send us to order the selected products. Simple and efficient. Welcome to Accipiter - the simplest webshop on Earth Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Accipiter - le webshop le plus simple Ce webshop vous permet de générer un email à nous envoyer pour commander les produits sélectionnés. Simple et efficace. Bienvenue sur Accipiter - le webshop le plus simple 