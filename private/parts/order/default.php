<div class="inner container-fluid mb-3">
    <div class="row justify-content-md-center">
        <div class="col-lg-6">
            <hr class="mb-4">
            <h1 class="mb-3"><?php echo _("Review your order") ?></h1>

            <form class="needs-validation" novalidate>
                <div class="mb-3">
                    <label for="orderNumber"><?php echo _("Order number") ?></label>
                    <input type="text" readonly class="form-control-plaintext" id="orderNumber" value="<?php echo $order_number ?>">
                </div>

                <div class="mb-3">
                    <label for="customerName"><?php echo _("Name") ?></label>
                    <input type="text" class="form-control" id="customerName" placeholder="" value="" required>
                    <div class="invalid-feedback">
                        Valid last name is required.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email"><?php echo _("Email") ?></label>
                    <input type="email" class="form-control" id="email" placeholder="you@example.com">
                    <div class="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                    </div>
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit"><?php echo _("Send your order") ?></button>
            </form>
        </div>
    </div>
</div>