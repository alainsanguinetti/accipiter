<div class="inner cover">
  <h1 class="cover-heading"><?php echo _("Welcome to Accipiter - the simplest webshop on Earth") ?></h1>
  <p class="lead"><?php echo _("A simple email-based webshop interface.") ?></p>
</div>