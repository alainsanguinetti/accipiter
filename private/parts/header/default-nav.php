<header class="masthead pb-5">
<div class="inner">
  <h3 class="masthead-brand"><?php echo $ini_array['name'] ?></h3>
  <nav class="nav nav-masthead justify-content-center">
    <?php if ($ini_array['main_website_uri']): ?>
      <a class="nav-link active" href="<?php echo $ini_array['main_website_uri'] ?>"><?php echo _("Back to website") ?></a>
    <?php endif ?>
  </nav>
</div>
</header>
