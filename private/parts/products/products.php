<div class="inner mx-auto" id="products">
    <hr class="mb-4">

    <h1 class="mb-3">
        <?php echo _("Choose products") ?>
    </h1>
    <p class="mb-3">
        <span class="badge badge-secondary badge-pill">3</span>
        <span class="text-muted"><?php echo _(" products selected") ?></span>
    </p>

    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-lg-10">
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Product name</h6>
                            <small class="text-muted">Brief description</small>
                        </div>
                        <span class="text-muted">$12</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Second product</h6>
                            <small class="text-muted">Brief description</small>
                        </div>
                        <span class="text-muted">$8</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Third item</h6>
                            <small class="text-muted">Brief description</small>
                        </div>
                        <span class="text-muted">$5</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                        <span>Total (USD)</span>
                        <strong>$20</strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
