<footer class="mastfoot pt-4">
  <div class="inner">
    <p><?php echo _("Webshop created with ") ?><a href="https://gitlab.com/alainsanguinetti/accipiter">Accipiter Nisus</a><?php echo _(", by ") ?><a href="https://eas-robotics.be/">EAS Robotics BV</a>.</p>
  </div>
</footer>