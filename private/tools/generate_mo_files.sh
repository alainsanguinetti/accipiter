#!/bin/bash

# directory of the script
cd "$(dirname "${BASH_SOURCE[0]}")"

# /private
cd ..

# update translations file
find ./locale -iname "*.po" -execdir msgfmt {} \;
