#!/bin/bash

# directory of the script
cd "$(dirname "${BASH_SOURCE[0]}")"

# /private
cd ..

# regenerate pot file
find . -iname "*.php" | xargs xgettext --from-code=UTF-8 --output=./locale/messages.pot

# update translations file
find ./locale -iname "*.po" -exec msgmerge --update {} ./locale/messages.pot \;

