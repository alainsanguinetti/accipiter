<?php
header("Content-type: text/html; charset=utf-8");

// Parse with sections
$ini_array = parse_ini_file("config.ini", True);

$locale = $ini_array['locale'];
setlocale(LC_MESSAGES, $locale);
bindtextdomain("messages", dirname(__FILE__)."/locale");
bind_textdomain_codeset("messages", 'utf-8');
textdomain("messages");


require('parts/page/begin.php');

require('parts/head/head.php');

require('parts/page/start_body.php');


require('parts/header/header.php');

require('parts/cover/cover.php');

require('parts/products/products.php');

require('parts/order/order.php');

require('parts/footer/footer.php');


require('parts/page/end_body.php');

require('parts/page/end.php');
?>
